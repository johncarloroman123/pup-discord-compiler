#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>

struct PostfixStack{
	char sym[20];
};
PostfixStack postfixstack[100];
static int postfixtop=-1;

struct RPNStack{
	int number;
	float fnumber;
};
RPNStack rpnstack[100];
static int rpntop=-1;


static int temptop=-1;
static int ivalue1=0;
static int ivalue2=0;
static float fvalue1=0;
static float fvalue2=0;

static int StackRPN[100];
static char tempstack[100],RPN[100];
//int solveExpression(char exp[100]);
void InitStack();
void ClearStack();
void Pop(char ch);
void pushToTemp(char ch);
void popFromTemp(char ch);
void PopRPNFloat(float value1, float value2,char ch);
void PopRPNInt(int value1, int value2,char ch);
void PushRPN(char ch);
int Empty();

int isfloat=0;

int Empty(){
return(temptop==-1); 		 //Check if stack is empty
}

void InitStack(){
	temptop=-1;              		//Initialize empty stack
}
void ClearStack(){
	temptop=-1;					//Clear Stack
}

void pushToTemp(char ch){
	temptop++;
	tempstack[temptop]=ch;			//Push character into stack
}

void popFromTemp(char ch){			//popFromTemp the top of the stack and concatenate to RPN
		
	postfixtop++;
	postfixstack[postfixtop].sym[0]=ch;
	postfixstack[postfixtop].sym[1]='\0';
	tempstack[temptop]='\0';			
	temptop--;

}
int Operator(char ch){
	
	if(ch=='^')
	return 3;
	else if(ch=='*'||ch=='/'||ch=='%')
	return 2;
	else if(ch=='+'||ch=='-')
	return 1;
	else 
	return 0;
}
void PushRPN(char ch[20]){
	int num=0, total=0, ctr=0, dp=0;
	float ftotal=0,ftemp=1,dec=0;
	
	for(int i=0; i<strlen(ch);i++){
		if(isdigit(ch[i])){
			num=ch[i]-'0';
				if(ch[i]!='.')
				total=(total*10)+num;
		}
		else {
			isfloat=1;
			ctr=i;
			break;
		}
	}
	if(ctr!=0){
		ctr++;
		
		for(int j=ctr; j<strlen(ch);j++){
			
			num=ch[j]-'0';
				dec=(dec*10)+num;
				
			dp++;

				
		}
		while(dp>0){
		ftemp*=10;
		dp--;
		}
		ftotal+=(dec/ftemp);
	
		rpntop++;
		rpnstack[rpntop].fnumber=ftotal+total;
	}
	else{
	rpntop++;
	rpnstack[rpntop].number=total;
	}
}
void PopRPNInt(int value1, int value2, char ch[5]){
	int num=0;

	if(strcmp(ch,"^")==0){
		num=pow(value1,value2);
	}
	else if(strcmp(ch,"*")==0){
		num=value1*value2;
	}
	else if(strcmp(ch,"/")==0){
		num=value1/value2;
	}
	else if(strcmp(ch,"%")==0){
		num=value1%value2;
	}
	else if(strcmp(ch,"+")==0){
		num=value1+value2;
	}
	else if(strcmp(ch,"-")==0){
		num=value1-value2;
	}
	rpntop++;
	rpnstack[rpntop].number=num;
}
void PopRPNFloat(float value1, float value2, char ch[5]){
	float num=0;
	

	if(strcmp(ch,"^")==0){
		num=pow(value1,value2);
	}
	else if(strcmp(ch,"*")==0){
		num=value1*value2;
	}
	else if(strcmp(ch,"/")==0){
		num=value1/value2;
	}
	//else if(strcmp(ch,"%")==0){
	//	num=value1%value2;
	//}
	else if(strcmp(ch,"+")==0){
		num=value1+value2;
	}
	else if(strcmp(ch,"-")==0){
		num=value1-value2;
	}
	rpntop++;
	rpnstack[rpntop].fnumber=num;

}

/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/
/*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+* I N T  M A I N  *+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*+*/
/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/
//int solveExpression(char exp[100]){
char* postfix (char value[256]){

	char ch, num[20], op[20], exp[100];
	int length,n=0,EXP[50],RPNlen,answer=0,ctr=0;
	strcpy(exp, value);
													//Enter arithmetic expression (string)
	length=strlen(exp);
	
	
	
	InitStack();		//Initialize Stack
	
											//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
											//	FIRST STEP: Algorithm to convert an infix expression to RPN	 //
											//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

//...................................................................................................................................................................//	
		
		for(n=0;n<length;n++){				//==================================//
			if(exp[n]=='('){				//	If token is temptop left parenthesis//
				ch=exp[n];					//		push it onto the stack		//
			pushToTemp(ch);						//									//
											//==================================//
			}//'('
//...................................................................................................................................................................//			
			else if(exp[n]==')'){
																			//==========================================================//
				while(tempstack[temptop]!='('){									//															//
					ch=tempstack[temptop];										//	If token is a right parenthesis... Pop and concantenate	//
					popFromTemp(ch);												//		stacks elements to RPN until a left parenthesis is 	//
					
					if(tempstack[0]=='\0'){									//			popped, but do not concatenate it to RPN.		//
					getch();													//		left parenthesis found... It is an ERROR			//
					return 0;													//															//
					}															//==========================================================//
				}
				
				temptop--;
			
			}//')'
//...................................................................................................................................................................//			
			else if(exp[n]=='^'||exp[n]=='%'||exp[n]=='*'||exp[n]=='/'||exp[n]=='+'||exp[n]=='-'){
				
				if(Operator(exp[n])>Operator(tempstack[temptop])||tempstack[temptop]=='('||temptop==-1||postfixtop==-1){
																			//====================================================================================//	
																			//																					  //		
					ch=exp[n];												//	If the stack is empty or token has a higher priority than the top stack element,  // 
					pushToTemp(ch);												//	push token onto the stack. Otherwise, pop and concatenate the top stack element   //
					getch();															//																					  //
				}															//====================================================================================//
				else{
					ch=tempstack[temptop];
					popFromTemp(ch);
					ch=exp[n];
					pushToTemp(ch);
				}
				
				
				}//operators
			
//...................................................................................................................................................................//			
			else if(isdigit(exp[n])||exp[n]=='.'){
				while(isdigit(exp[n])||exp[n]=='.'){	
					num[ctr]=exp[n];
					n++;
					ctr++;
				}
				num[ctr]='\0';
				n--;
				ctr=0;										//================================================//
				postfixtop++;								//	If token is an operand concatenate it to RPN  //
				strcpy(postfixstack[postfixtop].sym,num);
				strcpy(num,"");
															//================================================//
	
			}//Operand
//...................................................................................................................................................................//			
		}//for
										//============================================================//
		while(tempstack[temptop]!=NULL){		//															  //
			ch=tempstack[temptop];			//	When the end of the infix expression is reached, pop and  //
			popFromTemp(ch);					//	concatenate stack items to RPN until the stack is empty.  //
										//															  //
										//============================================================//
		}
		
		ClearStack();		//Clear the stack


	
	for(int i=0; i<=postfixtop; i++){
		

		
		if((postfixstack[i].sym,"^")==0||strcmp(postfixstack[i].sym,"*")==0||strcmp(postfixstack[i].sym,"/")==0||strcmp(postfixstack[i].sym,"%")==0
		||strcmp(postfixstack[i].sym,"+")==0||strcmp(postfixstack[i].sym,"-")==0){
			strcpy(num,postfixstack[i].sym);
			if(isfloat==0){
			ivalue2=rpnstack[rpntop].number;
			rpntop--;
			ivalue1=rpnstack[rpntop].number;
			rpntop--;
			PopRPNInt(ivalue1,ivalue2,num);
			}
			else{
				
			fvalue2=rpnstack[rpntop].fnumber;
			rpntop--;
			fvalue1=rpnstack[rpntop].fnumber;
			rpntop--;
			PopRPNFloat(fvalue1,fvalue2,num);
			}
				
			
		}
		else{
			strcpy(num,postfixstack[i].sym);
			PushRPN(num);
			strcpy(num,"");
		}
	}
	char result[256] = "";
	if(isfloat==0)
	{
		sprintf(result, "%d", rpnstack[rpntop].number);
		return result;
	}
	else{
		isfloat=0;
		sprintf(result, "%f", rpnstack[rpntop].fnumber);
		return result;	
	}
	//return StackRPN[rpntop];
	getch();
}
