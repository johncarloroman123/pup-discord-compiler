#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<malloc.h>
#include<ctype.h>
#include<string.h>

struct node {
	char data;
	struct node *next;

} *head1, *tail1, *current1;
// head, tail, current - Linked List for Code.txt (text editor for code input)

struct grammar {
	char data;
	struct grammar *next;

} *head2, *tail2, *current2;
// head2, tail2, current2 - Linked List for the Rules.txt (rules are defined)

// for keywords and reserved words (rules defined by the group)
struct keyword {
	char data;
	int lvl_indent;
	int line;
	int position;
	struct keyword *next;
};

// for tab sensitive programming language
int line = 1, lvl_indent = 0;

char c, integer[100];

// DEFINED FUNCTIONS FOR LEXICAL ANALYSIS
void insertIntoLinkedList(char data);	
// insert the sample code of the file into a linked list
void LexicalAnalyzing();
//checks for keywords, constants, operators from linked list character by character

// states
void isIdentifier();
void isStringConstant();
void error();

// for debugging purposes to see whether the pre-defined function above is correct
void viewLinkedList();
void viewGrammarTable();

void lexi() {
	char ch;
	FILE *fp;
	fp = fopen("Code.d", "r");
	while ((ch = fgetc(fp)) != EOF) {
		insertIntoLinkedList(ch);
	 }
	fclose(fp);

	LexicalAnalyzing();
}

void insertIntoLinkedList(char data) 
{
	current1 = (node *) malloc(sizeof(node)); //creates a linked list, then allocates memory cells to accept input from Code.txt
	current1->data = data;
	current1->next = NULL;

	if (head1 == NULL) {
		head1 = current1;
	}
	else {
		tail1->next = current1;
	}
	tail1 = current1;
}
void LexicalAnalyzing() 
{
	FILE *fpSymbol;
	fpSymbol = fopen("Output.txt","w");
	
	fclose(fpSymbol);
	// let the current get the address of the head
	current1 = head1;
	
	// if the current is equal to NULL exit loop
	while(current1 != NULL) 
	{	int c;
		fpSymbol = fopen("Output.txt","a");
		c = current1->data;
		// if the current->data is a newline or \n
		if(current1->data == '\n') 
		{
			line++;
			lvl_indent = 0;
			current1 = current1->next;
		} // endif \n

		// if the current->data is tab or \t
		else if(current1->data == '\t') 
		{
			lvl_indent++;
			current1 = current1->next;
		} // endif \t
		
		// if the current->data is ' ' or space
		else if (current1->data == ' ') 
		{
			current1 = current1->next;
		}
		
		else if (isupper(c))
		{
			isIdentifier();
		}
		// if it is a number
		else if (isdigit(current1->data)) 
		{
			num:
			int isFloat = 0, checker=0, index=0;
			for (index=0;index<20; index++)
			{
				integer[index]='\0';
			}
			index=0;
			do 
			{
				
				if (isdigit(current1->data))
				{
					checker=0;
					
					integer[index]=current1->data;
					index++;
				}
					
				else if (current1->data == '.')
				{
					isFloat=isFloat+1;
					
					integer[index]=current1->data;
					index++;
					
				}
				else
				{
					checker=1;
					error();
					for (index=0;index<100; index++)
					{
						integer[index]='\0';
					}
				}
			
				//fprintf(fpSymbol,"%c", current1->data);
				current1 = current1->next;	
			} while (current1->data != ' ' && current1->data != ')' && current1->data != '\n'
			 	&& current1->data != '\t');
			if (isFloat == 1 && checker==0) 
			{
				
				//printf("\n-------------------------------\n");
				int len=0;
				len=strlen(integer);
				for(int i=0; i<len; i++)
				{
					fprintf(fpSymbol,"%c", integer[i]);
				}
				fprintf(fpSymbol,"\tfloat_constant\t %d\n", line);
			}
			else if (checker==0)
			{
			
				//printf("\n-------------------------------\n");
				int len=0;
				len=strlen(integer);
				for(int i=0; i<len; i++)
				{
					fprintf(fpSymbol,"%c", integer[i]);
				}
				fprintf(fpSymbol,"\tint_constant\t %d\n",line);
			}
		
		}
		// keywords are if, input, int 
		else if(current1->data == 'i') 
		{	
			
			current1 = current1->next;
			if(current1->data == 'f')
			{
				
				current1=current1->next;
				if (current1->data!=' ')
				{
					error();
				}
				else
				{
					
					fprintf(fpSymbol,"if\tif_stmt\t%d\n", line);
					keyword *head, *tail, *current; 
					current1 = current1->next;
					////viewKeywords(head, tail, current);

				}
			} // endif f
			else if(current1->data == 'n') 
			{
			 	
				current1 = current1->next;
				if(current1->data == 't') 
				{
					
					current1=current1->next;
					if (current1->data!=' ')
						error();
					else
					{
					
						fprintf(fpSymbol,"int\tint_type\t%d\n", line);
						keyword *head, *tail, *current;
						////viewKeywords(head, tail, current);
						current1 = current1->next;
					}	
				} // endif t		
				else 
				{          
					error();
				} // endelse
			} // endif n
			else if(current1->data == 'O') 
			{
			 	
				current1 = current1->next;
				if(current1->data == 'u') 
				{
					
					current1=current1->next;
					if (current1->data == 't')
					{
						
						current1=current1->next;
						if(current1->data == 'p')
						{
							
							current1=current1->next;
							if(current1->data == 'u')
							{	
								
								current1=current1->next;
								if(current1->data == 't')
								{
									
									current1=current1->next;
									if(current1->data != ' ')
									{
										error();
									}
									else
									{
										
										fprintf(fpSymbol,"iOutput\toutput_stmt\t%d\n",line);
										keyword *head, *tail, *current;
										//viewKeywords(head, tail, current);
										current1 = current1->next;
									}
								}
								else
								{
									error();
								}
							}
							else
							{
								error();
							}
						}
						else
						{
							error();
						}
					}
					else
					{
						error();
					}	
				} // endif t
				else
					error();				
			} // endif i
			else if(current1->data == 'S') 
			{
			 	
				current1 = current1->next;
				if(current1->data == 'c') 
				{
					
					current1=current1->next;
					if (current1->data == 'a')
					{
						
						current1=current1->next;
						if(current1->data == 'n')
						{
							
							current1=current1->next;
							if(current1->data != ' ')
							{
								error();
							}
							else
							{
								
								fprintf(fpSymbol,"iScan\tinput_stmt\t%d\n",line);
								keyword *head, *tail, *current;
								//viewKeywords(head, tail, current);
								current1 = current1->next;
							}
						}
						else
						{
							error();
						}
					}
					else
					{
						error();
					}
				}	
				else
				{
					error();
				}
			}
		}	  
		else if (current1->data=='t')
		{
			
			current1=current1->next;
			if (current1->data=='r')
			{
				
				current1=current1->next;
				if(current1->data=='u')
				{
					
					current1=current1->next;
					if(current1->data=='e')
					{
						
						current1=current1->next;
						if(current1->data!=' ')
						{
							error();
						}
						else
						{
							
							fprintf(fpSymbol,"true\tboolean_constant\t%d\n",line);
							keyword *head, *tail, *current;
							//viewKeywords(head, tail, current);
							current1 = current1->next;
						}
					}
					else
					{
						error();
					}
				}
				else
				{
					error();
				}
			}
			else
			{
				error();
			}
		}
		else if(current1->data == 'e') 
		{
			
			current1 = current1->next;
			if(current1->data == 'l') 
			{
				
				current1 = current1->next;
				if(current1->data == 's') 
				{
					
					current1 = current1->next;
					
					if (current1->data == 'i')
					{
						
						current1 = current1->next;
						if (current1->data == 'f')
						{
							
							current1 = current1->next;
							if(current1->data != ' ') 
							{
								error();
							} // endif e
							else 
							{
							
								fprintf(fpSymbol,"elsif\telsif_stmt\t%d\n", line);
								keyword *head, *tail, *current;
								//viewKeywords(head, tail, current);
								current1 = current1->next;
							}// endelsif
						}
						
						else
						{
							error();
						}
					}
					
					else if (current1->data != ' ')
					{
						error();
					}
					else 
					{
				
						fprintf(fpSymbol,"els\tels_stmt\t%d\n", line);
						keyword *head, *tail, *current;
						//viewKeywords(head, tail, current);
						current1 = current1->next;
					}// endelse
				} // endif s
				
				else 
				{
					error();
				} //endelse
			} //endif l
			else if (current1->data=='q')
			{
					
					current1 = current1->next;
					if (current1->data=='u')
					{
						
						current1 = current1->next;
						if (current1->data=='a')
						{
							
							current1 = current1->next;
							if (current1->data=='l')
							{
								
								current1 = current1->next;
								if (current1->data=='s')
								{
									
									current1=current1->next;
									if (current1->data!=' ')
										error();
									else 
									{
								
										fprintf(fpSymbol,"equals\tequ_opr\t%d\n", line);
										keyword *head, *tail, *current;
										//viewKeywords(head, tail, current);
										current1 = current1->next;
									}
								}
							} 
							else 
							{
								error();
							}	 // endelse
						} 
						else 
						{
								error();
						}	 // endelse
					} 
					else {
							error();
							}	 // endelse
				}
				else {
					error();
				} // endelse
			} // endif e
		else if (current1->data=='b')
		{
			
			current1=current1->next;
			if (current1->data=='o')
			{
				
				current1=current1->next;
				if (current1->data=='o')
				{
					
					current1=current1->next;
					if (current1->data=='l')
					{
						
						current1=current1->next;
						if(current1->data!=' ')
						{
							error();
						}
						else
						{
					
							fprintf(fpSymbol,"bool\tboolean_type\t%d\n",line);
							keyword *head, *tail, *current;
							//viewKeywords(head, tail, current);
							current1 = current1->next;
						}
					}
					else
					{
						error();
					}
				}
				else
				{
					error();
				}
			}
			else
			{
				error();
			}
		}

		//keyword is float
		else if(current1->data == 'f')
		{
			
			current1 = current1->next;
			if(current1->data == 'l') 
			{
				
				current1 = current1->next;
				if(current1->data == 'o') 
				{
					
					current1 = current1->next;
					if(current1->data == 'a') 
					{
						
						current1 = current1->next;
						if(current1->data == 't') 
						{
							
							current1 = current1->next;
							if (current1->data!=' ')
								error();
							else 
							{
						
								fprintf(fpSymbol,"float\tfloat_type\t%d\n",line);
								keyword *head, *tail, *current;
								//viewKeywords(head, tail, current);
								current1 = current1->next;
							}
						} // endif t
						else 
						{
							error();
						} // endelse
					} // endif a
					else 
					{
						error();
					} // endelse
				} // endif o
				else 
				{
					error();
				} // endelse
			} // endif l
			else if (current1->data=='a')
			{
				
				current1=current1->next;
				if (current1->data=='l')
				{
					
					current1=current1->next;
					if (current1->data=='s')
					{
						
						current1=current1->next;
						if(current1->data=='e')
						{
							
							current1=current1->next;
							if (current1->data!=' ')
							{
								error();
							}
							else
							{
							
								fprintf(fpSymbol,"false\tboolean_constant\t%d\n", line);
								keyword *head, *tail, *current;
								//viewKeywords(head, tail, current);
								current1 = current1->next;	
							}
						}
						else
						{
							error();
						}
					}
					else
					{
						error();
					}
				}
				else
				{
					error();
				}
			}
			else 
			{
				error();
			} // endelse
		} // endif f. for float
		
		else if (current1-> data == 'w')
		{
			
			current1 = current1 -> next;
			if(current1 -> data == 'h')
			{
				
				current1 = current1 -> next;
				if(current1 -> data == 'l')
				{
				
					current1 = current1 -> next;
					if(current1 -> data != ' ')
					{
						isIdentifier();
					}
					else
					{
						
						fprintf(fpSymbol,"whl\tloop_stmt\t%d\n", line);
						keyword *head, *tail, *current;
						//viewKeywords(head, tail, current);
						current1 = current1->next;
					}
				}
			}
			else
			{
				error();
			}
		}
		else if(current1->data == '+')
		{
			
			current1=current1->next;
			if (current1->data!=' ')
			{
				error();
			}
			else
			{
			
				fprintf(fpSymbol,"+\tadd_opr\t%d\n",line);
				keyword *head, *tail, *current;
				//viewKeywords(head, tail, current);
				current1 = current1->next;	
			}
			
		}
		
		else if(current1->data == '^')
		{
		
			current1=current1->next;
			if(current1->data=='^')
			{
				
				current1=current1->next;
				if (current1->data!=' ')
					error();
				else
				{
			
					fprintf(fpSymbol,"^^\texp_opr\t%d\n",line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
				
				
			}
			else
			{
				if (current1->data!=' ')
					error();
				else
				{
			
					fprintf(fpSymbol,"^\tmul_opr\t%d\n",line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
				
			}
		}
		
		else if(current1->data == '/')
		{
			
			current1 = current1 ->next;
			if (current1->data=='/')
			{
				current1=current1->next;
				if (current1->data!= ' ')
					error();
				else
				{
			
					fprintf(fpSymbol,"//\tintdiv_opr\t%d\n",line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
					
			}
			else
			{
				if (current1->data!=' ')
					error();
				else
				{
					fprintf(fpSymbol,"/\tdiv_opr\t%d\n", line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
				
			}
		}
		
		// keyword is str, sub (operator)
		else if(current1->data == 's') 
		{
			
			current1 = current1->next;
			if(current1->data == 't') 
			{
				
				current1 = current1->next;
				if(current1->data == 'r') 
				{
					
					current1=current1->next;
					if (current1->data!=' ')
						error();
					else 
					{
						fprintf(fpSymbol,"str\tstring_type\t%d\n",line);
						keyword *head, *tail, *current;
						//viewKeywords(head, tail, current);
						current1 = current1->next;
					}
				} // endif r
				else 
				{
					error();
				} // endelse
			} // endif t
		} // endif s for string and sub
		
		else if (current1 -> data == '.')
		{
			
			current1 = current1->next;
			if(current1 -> data == '&')
			{
				current1=current1->next;
				if(current1->data!=' ')
					error();
				else
				{
					fprintf(fpSymbol,".&\tlog_and_opr\t%d\n", line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
					 
				
			}
			
			else if (current1 -> data == '|')		  
			{
				current1=current1->next;
				if(current1->data!=' ')
					error();
				else
				{
					fprintf(fpSymbol,".|\tlog_or_opr\t%d\n",line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next; 	
				}
				
				
			}
			else
			{
				error();
			}
		}
		// Delimiter is (
		else if(current1->data == '(') 
		{
			current1=current1->next;
			if (current1->data!=' ')
				error();
			else
			{
				fprintf(fpSymbol,"(\tlef_paren\t%d\n", line);
				keyword *head, *tail, *current;
				//viewKeywords(head, tail, current);
				current1 = current1->next; 
			}
				
		}// endif (

		// Delimiter is )
		else if(current1->data == ')') 
		{
			current1=current1->next;
			if (current1->data!=' ')
				error();
			else
			{
				fprintf(fpSymbol,")\tright_paren\t%d\n", line);
				keyword *head, *tail, *current;
				//viewKeywords(head, tail, current);
				current1 = current1->next;
			}
			
		}// endif )
		
		

		// Delimiter is [ 
		else if(current1->data == '[') 
		{
			current1=current1->next;
			if (current1->data!=' ')
				error();
			else
			{
				fprintf(fpSymbol,"[\tlef_brac\t%d\n", line);
				keyword *head, *tail, *current;
				//viewKeywords(head, tail, current);
				current1 = current1->next; 
			}
				
		}// endif [
		else if(current1->data == ']') 
		{
			current1=current1->next;
			if (current1->data!=' ')
				error();
			else
			{
				fprintf(fpSymbol,"]\tright_brac\t%d\n", line);
				keyword *head, *tail, *current;
				//viewKeywords(head, tail, current);
				current1 = current1->next; 
			}
			
		} //endif ]
		
		
		// Delimiter is "
		else if(current1->data == '"') 
		{
			fprintf(fpSymbol,"%c\tdbl_quot\t%d\n",current1->data,line);
			keyword *head, *tail, *current;
			//viewKeywords(head, tail, current);
			current1 = current1->next;
			isStringConstant();
		} // endif "

		// Line Comment is ??
		else if(current1->data == '*') {
			
			current1 = current1->next;
			if (current1->data == '*')
			{
				
				current1=current1->next;
				if (current1->data!=' ')
					error();
				else{
						//fprintf(fpSymbol,"*\t\tline comment\n");
						keyword *head, *tail, *current;
						//viewKeywords(head, tail, current);
						current1 = current1->next;
					do {
						
						current1 = current1->next;
					} while (current1->data != '\n');
					}
			}//endif ?
		} // endif ?
		else if(current1->data == 'm') 
		{
			
			current1 = current1->next;
			if(current1->data == 'o') 
			{
				
				current1 = current1->next;
				if (current1->data=='d')
				{
					
					current1 = current1->next;
					if (current1->data=='u')
					{
						
						current1 = current1->next;
						if (current1->data=='l')
						{
							
							current1 = current1->next;
							if (current1->data=='o')
							{
								
								current1 = current1->next;
								if (current1->data!=' ')
									error();
								else
								{
									fprintf(fpSymbol,"modulo\tmdls_opr\t%d\n",line);
									keyword *head, *tail, *current;
									//viewKeywords(head, tail, current);
									current1 = current1->next;
								}
							}//endif n
							else 
							{
								error();
							} // endelse
						}//endif i
						else 
						{
							error();
						} // endelse
					}//endif a
					else 
					{
						error();
					} // endelse
				}//endif m
				else 
				{
					error();
				} // endelse
			}//endif e
			else 
			{
				error();
			} // endelse
		}//endif r
			
		// Operator is ->
		else if(current1->data == '-') 
		{
			
			current1 = current1->next;
			if (current1->data == '>')
			{
				
				current1=current1->next;
				if (current1->data!=' ')
				{
					error();
				}
				else 
				{
					fprintf(fpSymbol,"->\tassign_opr\t%d\n", line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
			}
			else if (current1 -> data == ' ')
			{
				fprintf(fpSymbol,"-\tsubt_opr\t%d\n", line);
				keyword *head, *tail, *current;
				//viewKeywords(head, tail, current);
				current1 = current1->next;
			}
			
			else if (isdigit(current1->data)) 
		{	
			
			int isFloat = 0, checker=0, index=0;
			for (index=0;index<20; index++)
			{
				integer[index]='\0';
			}
			index=0;
			do 
			{
				
				if (isdigit(current1->data))
				{
					checker=0;
					
					integer[index]=current1->data;
					index++;
				}
					
				else if (current1->data == '.')
				{
					isFloat=isFloat+1;
					
					integer[index]=current1->data;
					index++;
					
				}
				else
				{
					checker=1;
					error();
					for (index=0;index<100; index++)
					{
						integer[index]='\0';
					}
				}
			
				//fprintf(fpSymbol,"%c", current1->data);
				current1 = current1->next;	
			} while (current1->data != ' ' && current1->data != ')' && current1->data != '\n'
			 	&& current1->data != '\t');
			if (isFloat == 1 && checker==0) 
			{
				//printf("\n-------------------------------\n");
				int len=0;
				len=strlen(integer);
				fprintf(fpSymbol,"-");
				for(int i=0; i<len; i++)
				{
					fprintf(fpSymbol,"%c", integer[i]);
				}
				fprintf(fpSymbol,"\tfloat_constant\t %d\n", line);
			}
			else if (checker==0)
			{
				//printf("\n-------------------------------\n");
				int len=0;
				len=strlen(integer);
				fprintf(fpSymbol,"-");
				for(int i=0; i<len; i++)
				{
					fprintf(fpSymbol,"%c", integer[i]);
				}
				fprintf(fpSymbol,"\tint_constant\t %d\n",line);
			}
		
		}
			else
			{
				error();
			} // endelse
		} // endif ->
		
		// Operator is !! and !equals
		else if(current1->data == '!') 
		{
			
			current1 = current1->next;
			if (current1->data == '!')
			{
				
				current1 = current1->next;
				if (current1 -> data == ' ')
				{
					fprintf(fpSymbol,"!!\tlog_not-opr\t%d\n", line);
					keyword *head, *tail, *current; 
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
				else if (current1->data =='e')
				{
					
					current1 = current1->next;
					if (current1 -> data == 'q')
					{
						
						current1 = current1->next;
						if (current1 -> data == 'u')
						{
							
							current1 = current1 -> next;
							if (current1 -> data == 'a')
							{
								
								current1 = current1-> next;
								if (current1 -> data == 'l')
								{
									
									current1 = current1 -> next ;
									if (current1 -> data == 's')
									{
										
										current1 = current1 -> next;
										if(current1 -> data != ' ')
										{
											error();
										}
										else
										{
											fprintf(fpSymbol,"!equals\tnot_eq_opr\t%d\n", line);
											keyword *head, *tail, *current;
											//viewKeywords(head, tail, current);
											current1 = current1->next;
										}
									}
									else
									{
										error();
									}
								}
							}
							else
							{
								error();
							}
						}
						else
						{
							error();
						}
					}
					else
					{
						error();
					}
				}	
			}
			else 
			{
				error();
			} // endelse
		} // endif !! and !equals
		
		// Operator is << and <eql
		else if(current1->data == '<') 
		{
			
			current1 = current1->next;
			if (current1->data == '<')
			{
				
				current1 = current1->next;
				if (current1->data!=' ')
					error();
				else
				{
					fprintf(fpSymbol,"<<\tlssthn_opr\t%d\n",line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
			}
			else if (current1->data=='e')
			{
				
				current1 = current1->next;
				if (current1->data=='q')
				{
					
					current1 = current1->next;
					if (current1->data=='l')
					{
						
						current1 = current1->next;
						if (current1->data!=' ')
							error();
						else
						{
							fprintf(fpSymbol,"<eql\tlsstn_eq_opr\t%d\n", line);
							keyword *head, *tail, *current;
							//viewKeywords(head, tail, current);
							current1 = current1->next;
						}
					}
				}
				else 
				{
					error();
				} // endelse
			}
			else 
			{
				error();
			} // endelse
		} // endif << and <eql
		else if (current1->data=='d')
			{
				
				current1 = current1->next;
				if (current1->data=='s')
				{
					
					current1 = current1->next;
					if (current1->data=='c')
					{
						
						current1 = current1->next;
						if (current1->data=='r')
						{
							
							current1 = current1->next;
							if (current1->data=='d')
							{
								
								current1 = current1->next;
								if(current1->data!=' ')
								{
									error();
								}
								else
								{
									fprintf(fpSymbol,"dscrd\tnoise_word\t%d\n",line);
									keyword *head, *tail, *current;
									//viewKeywords(head, tail, current);
									current1 = current1->next;
								}
							}
							else
							{
								error();
							}
						}
						else
						{
							error();
						}
					}
					else
					{
						error();
					}
				}
				else
				{
					error();
				}
			}
		// Operator is >> and >eql
		else if(current1->data == '>') 
		{
			
			current1 = current1->next;
			if (current1->data == '>')
			{	
				current1 = current1->next;
				if (current1->data!=' ')
					error();
				else
				{
					fprintf(fpSymbol,">>\tgrtrthn_opr\t%d\n",line);
					keyword *head, *tail, *current;
					//viewKeywords(head, tail, current);
					current1 = current1->next;
				}
			}
			
			else if (current1->data=='e')
			{
				
				current1 = current1->next;
				if (current1->data=='q')
				{
					
					current1 = current1->next;
					if (current1->data=='l')
					{
						
						current1 = current1->next;
						if (current1->data!=' ')
							error();
						else
						{
							
							fprintf(fpSymbol,">eql\tgrtrthn_eql_opr\t%d\n",line);
							keyword *head, *tail, *current;
							//viewKeywords(head, tail, current);
							current1 = current1->next;
						}
					}
				}
				else 
				{
						error();
				} // endelse
			}
			else 
			{
						error();
			} // endelse
		} // endif << and <eql
		
		
		
		else 
		{
			error();
		} // endelse if word is user-defined identifier
	fclose(fpSymbol);
	} // end of while loop
} // end of LexicalAnaylzing
/* checking purposes */
void isIdentifier() 
{
	FILE *fpSymbol;
	fpSymbol = fopen("Output.txt", "a");
	do {
		
		//c = current1->data;
		fprintf(fpSymbol,"%c", current1->data);
		current1 = current1->next;
	}while(current1->data != ' '  && current1->data != '\n' && current1->data != '\t');
	fprintf(fpSymbol,"\tusrdfn_id\t%d\n",line);
	
	fclose(fpSymbol);
} // end of isIdentifier
void isStringConstant() 
{
	FILE *fpSymbol;
	fpSymbol = fopen("Output.txt", "a");
	fprintf(fpSymbol,"\"\tdbl_quot\t%d\n",line);
	do {
		
		fprintf(fpSymbol,"%c",current1->data);
		current1 = current1->next;        
		//if(current1->data == '\"' || current1->data == '\'') {
		//}
	}while(current1->data != '\"' && current1->data != '\'');
	
			
			fprintf(fpSymbol,"\tstring_constant\t%d\n",line);
			
			
	keyword *head, *tail, *current;
	if (current1->data == '"') {
		current1 = current1->next;
		//viewKeywords(head, tail, current);
	} // endif "
	
	
	fclose(fpSymbol);
} // end of isStringConstant
void error()
{
	
	do
	{	
		if (current1->data==' ') break;
		current1=current1->next;
	}while (current1->data != ' ' && current1->data != '\n' && current1->data != '\t');
	printf("\t-\tError in line %d, column %d\n", line, lvl_indent);
	
}





