#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<malloc.h>
#include<ctype.h>
#include<string.h>
#include"lexir.h"
#include<malloc.h>
#include "postfix.h"
#include "relational.h"
int isNonTerminal(char str[100]);
char* separatedProd;
void readSyntaxFromLexi(int index);

struct tokens
{
	char token[256];
	char lexeme[256];
	int linenum;
}tokenz, symToken;


struct Stack 
{
	char prod[100];
	char type[256];		
};

struct SymbolTable
{
	char lexval[20];
	char type[10];
	char val[50];
	int linenum;
	int use;
	int scope;
};

Stack stack[100];
SymbolTable symStack[30];
int sTop = -1;
int top=-1;
int index = 0;
int stTop=-1;
char type[20];
int loc, assignloc;
int ifglob;
int ifflag = 1;
int ifdoneflag = 0;
const char* trml[] = {"usfdn_id", "int_constant", "float_constant", "string_constant", "boolean_constant", "if_stmt", "els_stmt", "elsif_stmt",
						  "int_type", "float_type", "boolean_type", "string_type", "lef_brac", "right_brac", "lef_paren", "right_paren", "dbl_quot", "assign_opr", "grtrthn_opr", "grtrthn_eq_opr",
						  "lssthn_opr", "lssthn_eq_opr", "equ_opr", "not_eq_opr", "output_stmt", "input_stmt", "loop_stmt", "add_opr", "subt_opr", "mul_opr",
						  "mdls_opr", "div_opr", "intdiv_opr", "log_not_opr", "log_and_opr", "log_or_opr", "noise_word", "$"};
							
const char* nonTrml[] = {"start", "input_list", "output_list", "output_args", "output_id", "constant", "declaration_stmt", "dec_args", "data_type", "assignment_stmt", "assign_args", "assign_list",
                            "arith_exp", "arith_list", "arith_opr", "if_cond", "conditional_stmt", "cond_args", "cond", "relational", "rel_list", "log_list", "relational_opr", "logical_opr",
							"whl_stmt", "loop_args", "stmts","noise_stmt"};
							
const char* action[] = {"64","1", "2", "3", "4", "4.1", "5", "6", "6.2", "7", "8", "8.2", "9", "10", "10.2", "11", "11.2", "11.3", "12", "12.2", "13", "13.2", "14", "14.2", "14.3", "15",
						"16", "17", "18", "19", "19.2", "20", "20.2", "21", "21.2", "22", "22.2", "23", "23.2", "24", "25", "25.2", "26", "26.2", "27", "28", "29", "30",
						"31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "41.1", "42", "43", "43.2", "44", "45", "45.2", "46", "46.2", "47", "48", "49", "50",
						"51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64"};								

typedef struct node1* NodePointer;
typedef struct node2* NodePointer1;
void insertIntoLinkedList(tokens token, NodePointer *);
void displayLinkedList(NodePointer current3);
void displayLinkedList1(NodePointer1);
void parseInput();
char* retVal(int row,int col, char pproductions[]);
int getCol(char str[100]);
int getRow(char str[100]);
void insertIntoLinkedList1(char word3[]);
void pop();
void push(char str[100]);
int isAction(char str[100]);
void Spop();
void Spush(char str[100]);
void pushSymbol(char str[100], char str2[100]);
void insertSymbol(char str[100], char str2[100]);
int checkSymbol(char str[100]);
void readSyntaxFromLexi2(int ind);
void checkUnusedSymbol();

int intval;
float floatval;
char strval[50];
char boolval[10];
void pushSymbol(char str[100], char str2[100], int use)
{
	stTop++;
	strcpy(symStack[stTop].lexval, str);
	strcpy(symStack[stTop].type, str2);
	symStack[stTop].use = use;
}

int checkSymbol(char str[100])
{
	int r=-1;
	for(int i=stTop; i>=0; i--)
	{
		if(strcmp(symStack[i].lexval,str)==0)
		r=i;
	}
	return r;
}

int isNonTerminal(char str[100])
{
	int chk = 0;
	for(int i=0; i<28; i++)
	{
		if(strcmp(str, nonTrml[i]) == 0)
		{
			chk = 1;		
		}
	}
	return chk;	
}



int isAction(char str[100])
{
	int achk = -1;
	for (int a = 0; a < 83; a++)
	{
		if(strcmp(str, action[a]) == 0)
		{
			achk = a;		
		}
	}
	return achk;
}

void pop()
{
	top--;
}
void push(char str[100])
{
	top++;
	strcpy(stack[top].prod,str);
}
int getCol(char str[100])
{
	char c[500];
	int x=0;
	char *theValue;
	FILE *file = fopen("gt3.csv","r");
	fgets(c,500,file);
	theValue=strtok(c,",");
	if(strcmp(theValue,str)==0)
	{
		fclose(file);
		return x;
	}
	for(x=0;x<=39;x++)
	{	
		theValue=strtok(NULL,",");
		if(strcmp(theValue,str)==0)
		{
			break;
		}
	}
	fclose(file);
	return x;
}

int getRow(char str[100])
{
	char c[500];
	int x=0;
	char *theValue;
	FILE *file = fopen("gt3.csv","r");
	for(x=0;x<29;x++)
	{
		fgets(c,500,file);
		theValue=strtok(c,",");
		if(strcmp(theValue,str)==0)
		{
			break;
		}
	}	
	fclose(file);
	return x;
}
char* retVal(int row,int col)
{
	char c[500];
	FILE *file = fopen("gt3.csv","r");
	static char * theValue;
	for(int x=0;x<row;x++)
	{
		fscanf(file, "%*[^\n]\n", NULL);
		
	}
	fgets(c,500,file);	
	theValue=strtok(c,",");
	for(int a=0;a<=col;a++)
	{
		theValue=strtok(NULL,",");
	}
	fclose(file);
	return theValue;
}

int main()
{
	lexi();
	FILE *fpSymbol;
	fpSymbol = fopen("Output.txt", "a");
	int number = 5;
	fprintf(fpSymbol,"$\t$\t%d\n",number);
	fclose(fpSymbol);
	readSyntaxFromLexi(index);	
	parseInput();
	getch();
	return 5;
}

void readSyntaxFromLexi(int ind)
{
	FILE *reader;
	reader = fopen("Output.txt", "rw+");
	char line[256];
	tokens lexicals;
	int count = 0;
	while (reader != NULL)
	{
		fgets(line, sizeof(line), reader);
		if(count == index)
		{
		
			char * pch;
			int ctr = 0;
			pch = strtok(line, "\t");
			while(pch != NULL)
			{
				if(ctr == 0)
				{
					strcpy(lexicals.token, pch);
					ctr++;
				}
				else if(ctr == 1)
				{
					strcpy(lexicals.lexeme, pch);
					ctr++;
				}
				else
				{
					lexicals.linenum = atoi(pch);
					ctr++;
				}
				pch = strtok(NULL, "\t");
			}
			break;
		}
		count++;
    }
    strcpy(tokenz.token, lexicals.token);
    strcpy(tokenz.lexeme, lexicals.lexeme);
    tokenz.linenum = lexicals.linenum;
	fclose(reader);
}
void readSyntaxFromLexi2(int ind)
{
	FILE *reader;
	reader = fopen("Output.txt", "rw+");
	char line[256];
	tokens symTok;
	int count = 0;
	while (reader != NULL)
	{
		fgets(line, sizeof(line), reader);
		if(count == ind)
		{
		
			char * pch;
			int ctr = 0;
			pch = strtok(line, "\t");
			while(pch != NULL)
			{
				if(ctr == 0)
				{
					strcpy(symTok.token, pch);
					ctr++;
				}
				else if(ctr == 1)
				{
					strcpy(symTok.lexeme, pch);
					ctr++;
				}
				else
				{
					symTok.linenum = atoi(pch);
					ctr++;
				}
				pch = strtok(NULL, "\t");
			}
			break;
		}
		count++;
    }
    strcpy(symToken.token, symTok.token);
    strcpy(symToken.lexeme, symTok.lexeme);
    symToken.linenum = symTok.linenum;
	fclose(reader);
}
void parseInput()
{
	char LTokens[500] = "";
	char pusher[500]="$";
	char exp[256] = "";
	char printexp[256] = "";
	char relationalexp[256] = "";
	push(pusher);
	strcpy(pusher,"start");
	push(pusher);
	FILE * fp;
	fp = fopen("Syn.txt","w+");
	int assignloc;
	while(strcmp(stack[top].prod, "$")!=0)
	{
		if(isNonTerminal(stack[top].prod) == 1)
		{

			int row = getRow(stack[top].prod);
			int col = getCol(tokenz.lexeme);
			char *production=retVal(row,col);
			if(production == NULL)
			{
				strcat(LTokens, " ");					
				strcat(LTokens, tokenz.token);
				index++;
				readSyntaxFromLexi(index);				
			}
			else if(strcmp(production, "SYNCH") == 0)
			{
				strcat(LTokens, " ");					
			 	strcat(LTokens, tokenz.token);
				fprintf(fp, "Syntax Error at line: %d TOKEN: %s STACK TOP: %s\n", tokenz.linenum, tokenz.token, stack[top].prod);
				pop();
			}
			else
			{
								
				pop();
				int sby=0;
				char toString[2],pass[100]="";
				toString[1]='\0';
				sby=0;
				for(int i=strlen(production);i>=0;i--)
				{		
					if(production[i]==' '||i==0)
					{
						sby=i;
						if(production[i]==' ')
						i++;
						while(production[i]!=' '&&i!=strlen(production))
						{
							toString[0]=production[i];
							strcat(pass,toString);
							i++;
						}
						i=sby;
						push(pass);
						strcpy(pass,"");			
					}
				}
			}
		}
		else if (isAction(stack[top].prod) != -1)
		{
			if (ifflag == 0)
			{
				if (ifdoneflag == 0)
				{				
					if (strcmp(stack[top].prod, "40") == 0)
						ifflag = 1;
				}
				else 
				{
					if (strcmp(stack[top].prod, "37") == 0)
						{
							ifflag = 1;
							ifdoneflag = 0;
						}
				}
			}
			else if (strcmp(stack[top].prod, "1") == 0)
			{
				
			}
			
			else if (strcmp(stack[top].prod, "4") == 0)
			{
				readSyntaxFromLexi2(index - 2);
				loc = checkSymbol(symToken.token);
				scanf("%s", symStack[loc].val);	
			}	
			
			else if (strcmp(stack[top].prod, "4.1") == 0)
			{
				readSyntaxFromLexi2(index - 1);	
				loc = checkSymbol(symToken.token);			
				if(loc == -1)
				{
					fprintf(fp,"\nVariable %s not Declared", symToken.token);
				}
				else
				{
					symStack[loc].use = 1;
				}
			}
			
			else if (strcmp(stack[top].prod, "5") == 0)
			{
				printf("%s",printexp);
				strcpy(printexp, "");
			}
			
			else if (strcmp(stack[top].prod, "6") == 0)
			{
				readSyntaxFromLexi2(index - 1);	
				loc = checkSymbol(symToken.token);	
				if(loc == -1)
				{
					fprintf(fp,"\nVariable %s not Declared", symToken.token);
				}
				else
				{
					symStack[loc].use = 1;
					strcat(printexp, symStack[loc].val);
				}
			}	
			
			else if (strcmp(stack[top].prod, "7") == 0)
			{
				
			}	
			
			else if (strcmp(stack[top].prod, "8") == 0)		{
				readSyntaxFromLexi2(index - 1);
				loc = checkSymbol(symToken.token);				
				if(loc == -1)
				{
					fprintf(fp,"\nVariable %s not Declared", symToken.token);
				}
				else
				{
					symStack[loc].use = 1;
					strcat(printexp, symStack[loc].val);
				}
			}		
			

			else if (strcmp(stack[top].prod, "10") == 0)
			{
				strcpy(type, "int");
			}

			else if (strcmp(stack[top].prod, "11") == 0)
			{
				strcpy(type, "str");
			}

			else if (strcmp(stack[top].prod, "11.3") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				strcat(printexp, symToken.token	);
			}
				
			else if (strcmp(stack[top].prod, "12") == 0)
			{
				strcpy(type, "float");
			}
			
			else if (strcmp(stack[top].prod, "13") == 0)
			{
				strcpy(type, "bool");
			}

			else if (strcmp(stack[top].prod, "14.2") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				loc = checkSymbol(symToken.token);
				if(loc != -1)
				{
					fprintf(fp,"\nDouble Declaration\n");
				}
				else
				{	
					pushSymbol(symToken.token, type, 0);
					loc = checkSymbol(symToken.token);
				}
			}
			
			else if (strcmp(stack[top].prod, "14.3") == 0)
			{
			
			}
	
			else if (strcmp(stack[top].prod, "19") == 0)
			{
				strcpy(type, "int");

			}
	

			else if (strcmp(stack[top].prod, "20") == 0)
			{
				strcpy(type, "str");			
			}
			

			else if (strcmp(stack[top].prod, "21") == 0)
			{
				strcpy(type, "float");				
			}
		

			else if (strcmp(stack[top].prod, "22") == 0)
			{
				strcpy(type, "bool");
			}


			else if (strcmp(stack[top].prod, "23") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				loc = checkSymbol(symToken.token);
				assignloc = checkSymbol(symToken.token);
				if(loc == -1)
				{
					fprintf(fp,"\nVARIABLE %s NOT DECLARED", symToken.token);
				}
			}

			else if (strcmp(stack[top].prod, "23.2") == 0)
			{
				
			}
	
			else if (strcmp(stack[top].prod, "24") == 0)
			{
				strcpy(symStack[assignloc].val, postfix(exp));
			}

			else if (strcmp(stack[top].prod, "25") == 0)
			{
				if(loc == -1)
				{
					
				}
				else
				{				
					if(strcmp(symStack[loc].type, type) != 0)
					{
						fprintf(fp,"\nDATA TYPE MISMATCH FOR %s EXPECTED %s", symStack[loc].lexval, symStack[loc].type, type);
					}
					
					else
					{
						readSyntaxFromLexi2(index - 1);
						strcpy(symStack[loc].val, symToken.token);
						strcat(exp, symToken.token);											
					}
				}
			}

			else if (strcmp(stack[top].prod, "25.2") == 0)
			{
				
			}
			
			else if (strcmp(stack[top].prod, "26") == 0)
			{	
				readSyntaxFromLexi2(index - 1);
				loc = checkSymbol(symToken.token);
				if(loc == -1)
				{
					fprintf(fp,"\nVARIABLE %s NOT DECLARED", symToken.token);
				}
				
				else
				{
					int loc2 = checkSymbol(symToken.token);
					if(strcmp(symStack[loc].type, symStack[loc2].type) == 0)
					{
						symStack[loc].use = 1;
						strcat(exp, symStack[loc].val);
						
					}
					
					else
					{
						fprintf(fp,"\nDATA TYPE MISMATCH EXPECTED %s", symStack[loc].type);
					}
				}
			}

			else if (strcmp(stack[top].prod, "26.2") == 0)
			{
			
			}
	
			else if (strcmp(stack[top].prod, "27") == 0)
			{
			
			}

			else if (strcmp(stack[top].prod, "30") == 0 )
			{
				readSyntaxFromLexi2(index - 1);
				strcat(exp, symToken.token);
			}
			
			else if (strcmp(stack[top].prod, "31") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				strcat(exp, symToken.token);
			}
			
			else if (strcmp(stack[top].prod, "32") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				strcat(exp, "*");
			}
			
			else if (strcmp(stack[top].prod, "33") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				strcat(exp, symToken.token);
			}
			else if (strcmp(stack[top].prod, "37") == 0)
			{
				
			}	
											
			else if (strcmp(stack[top].prod, "38") == 0)
			{
			
			}

			else if (strcmp(stack[top].prod, "40") == 0 )
			{
				ifflag = 0;
				ifdoneflag = 1;
			}
			else if (strcmp(stack[top].prod, "41.1") == 0)
			{
				ifglob = relational(relationalexp);
				strcpy(relationalexp, "");
				if (ifglob == 1)
				{
					ifflag = 1;
				}
				else
				{
					ifflag = 0;
				}
			}

			else if (strcmp(stack[top].prod, "43") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				loc = checkSymbol(symToken.token);
				if(loc == -1)
				{
					fprintf(fp,"\nVARIABLE %s NOT DECLARED",symToken.token);
				}
				else
				{
					symStack[loc].use = 1;
					strcat(relationalexp, symStack[loc].val);
					strcat(relationalexp, " ");
				}
			}

			else if (strcmp(stack[top].prod, "43.2") == 0)
			{
			
			}
			
	
			else if (strcmp(stack[top].prod, "45") == 0)
			{
				loc = checkSymbol(symToken.token);
				if(loc  == -1)
				{
				
				}
				else
				{
					readSyntaxFromLexi2(index - 1 );
					int loc2 = checkSymbol(symToken.token);
					if(loc2 == -1)
					{
						fprintf(fp,"\nVARIABLE %s NOT DECLARED", symToken.token);
					}
					else
					{
						if(strcmp(symStack[loc].type, symStack[loc2].type) != 0 || strcmp(symStack[loc].type,"int") != 0 ||  strcmp(symStack[loc2].type,"int") != 0)
						{
							fprintf(fp,"\nDATA TYPE MISMATCH VARIABLE EXPECTED INT");
						}
						else
						{
							symStack[loc].use = 1;
							symStack[loc2].use = 1;
							strcat(relationalexp, symStack[loc].val);
						}
					}			
				}
			}									

			else if (strcmp(stack[top].prod, "45.2") == 0)
			{
			
			}

			else if (strcmp(stack[top].prod, "46") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				if(checkSymbol(symToken.token) == -1)
				{
					strcat(relationalexp, symToken.token);
					strcat(relationalexp, " ");
				}
				else
				{
					if(strcmp(type, "int") != 0)
					{
						fprintf(fp,"\nRULE 46 CONSTANT SHOULD BE INT");
					}
					else
					{
						if(strcmp(symStack[loc].type, type) != 0)
						{
							fprintf(fp, "\nRULE 46 DATA TYPE MISMATCH EXPECTED INT",symStack[loc].type);
						}
						else	
						{
							symStack[loc].use = 1;
							
						} 					
					}
				}
			}
			
			else if (strcmp(stack[top].prod, "46.2") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				loc = checkSymbol(symToken.token);
				strcat(relationalexp, symStack[loc].val);
				strcat(relationalexp, " ");
			}
			
			else if (strcmp(stack[top].prod, "49") == 0 )
			{
				readSyntaxFromLexi2(index - 1);
				strcat(relationalexp, symToken.token);
				strcat(relationalexp, " ");				
			}
			
			else if (strcmp(stack[top].prod, "50") == 0 )
			{
				readSyntaxFromLexi2(index - 1);
				strcat(relationalexp, symToken.token);
				strcat(relationalexp, " ");				
			}
			
			else if (strcmp(stack[top].prod, "51") == 0 )
			{
				readSyntaxFromLexi2(index - 1);
				strcat(relationalexp, symToken.token);
				strcat(relationalexp, " ");				
			}
			
			else if (strcmp(stack[top].prod, "52") == 0 )
			{
				readSyntaxFromLexi2(index - 1);
				strcat(relationalexp, symToken.token);
				strcat(relationalexp, " ");				
			}
			
			else if (strcmp(stack[top].prod, "53") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				strcat(relationalexp, symToken.token);
				strcat(relationalexp, " ");
			}
			
			else if (strcmp(stack[top].prod, "54") == 0)
			{
				readSyntaxFromLexi2(index - 1);
				strcat(relationalexp, symToken.token);
				strcat(relationalexp, " ");
			}
			
			else if (strcmp(stack[top].prod, "57") == 0)
			{
			
			}
	
			else if (strcmp(stack[top].prod, "58") == 0)
			{
			
			}									

			else if (strcmp(stack[top].prod, "59") == 0)
			{
			
			}

			else if (strcmp(stack[top].prod, "60") == 0)
			{
			
			}
			
			else if (strcmp(stack[top].prod, "61") == 0)
			{
			
			}

			else if (strcmp(stack[top].prod, "62") == 0)
			{
			
			}
	
			else if (strcmp(stack[top].prod, "63") == 0)
			{
			
			}
			else
			{
			
			}
			pop();
		}		
		else
		{
			if(strcmp(stack[top].prod,tokenz.lexeme)==0)
			{
							
				strcat(LTokens, " ");
				strcat(LTokens, tokenz.token);
				pop();
				index++;
				readSyntaxFromLexi(index);
			}
			else if(strcmp(stack[top].prod,"e")==0 || strcmp(stack[top].prod,"$") == 0 || strcmp(stack[top].prod," ")==0 || strcmp(tokenz.lexeme, "$") == 0)
			{
				pop();
			}
			else
			{
				strcat(LTokens, " ");
				strcat(LTokens, tokenz.token);
				fprintf(fp, "Syntax Error at line: %d TOKEN: %s STACK TOP: %s\n", tokenz.linenum, tokenz.token, stack[top].prod);
				index++; 
				readSyntaxFromLexi(index);
			}
		}
		fprintf(fp,"%s ", LTokens);
		for(int pi=top; pi>0; pi--)
		{
			fprintf(fp, "%s ",stack[pi].prod);
		}
		fprintf(fp,"PASSED \n");
		/*fprintf(fp,"\n\tSYMBOL TABLE\nLEXICAL\tTYPE\tVALUE\n");
		for(int i=stTop; i>=0; i--)
		{
			fprintf(fp,"%s\t%s\t%s\n", symStack[i].lexval, symStack[i].type, symStack[i].val);
		}*/
	}
	
	for(int i=stTop; i>=0; i--)
	{
		if(symStack[i].use ==0)
			fprintf(fp,"\nWARNING: VARIABLE %s NOT USED", symStack[i].lexval);
	}
	fclose(fp);
}

